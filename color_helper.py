import struct

def int_to_hex(i):
  hh = format(i, '02X')
  return ('\\x'+hh).decode('string_escape')

def rgb_to_hex(r, g, b):
  hr = int_to_hex(r)
  hg = int_to_hex(g)
  hb = int_to_hex(b)
  hcolor = hr+hg+hb
  return hcolor

def unpack_hex_string(hex_str):
  return struct.unpack('BBB',hex_str.decode('hex'))

def hex_color_to_hex_bytes(hex_str):
  try:
    (r,g,b) = unpack_hex_string(hex_str)
    c = rgb_to_hex(r, g, b)
    return c
  except:
    return None
