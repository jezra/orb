#!/usr/bin/env python2
#python libs
import sys
import signal
import threading
import time

#3rd party libs
import paho.mqtt.client as mqtt

#my own shitty lib type thingies
from spi import SPI
import GPIO
import settings

connected = False
testing = False

if len(sys.argv) > 1:
  testing = sys.argv[1] == "-t"

#create an spi instance
my_spi = SPI()
my_spi.testing = testing

def on_message(client, userdata, message):
  #split the message payload
  payload = message.payload.split("/")
  action = str(payload[0])
  #check for a value if the action is neither 'on' or 'off'
  if action != "on" and action != "off" and len(payload)>1:
    value = payload[1]
    if value=="on":
      value = settings.on_color
    elif value=="off":
      value = "000000"

  #what action is getting performed?
  if action == "on":
    my_spi.on()

  if action == "off":
    my_spi.off()

  elif action == "color":
    my_spi.color(value)

  elif action == "blink":
    ''' check for on_time and off_time values in seconds, or decimal portion of a second.
    examples 1, 10, 0.5, 0.01
    '''
    #are there 2 additional values?
    if len(payload) > 3 and payload[2].strip() != "" and payload[3].strip() != "":
      my_spi.blink(value, float(payload[2]), float(payload[3]))
    #is there 1 additional value?
    elif len(payload) > 2 and payload[2].strip() != "":
      my_spi.blink(value, float(payload[2]))

    else:
      my_spi.blink(value)

  elif action == "alert":
    #there may be extra data
    if len(payload) > 2 and payload[2].strip() != "":
      my_spi.alert(value, int(payload[2]))

    else:
      my_spi.alert(value)

  elif action == "fade":
    #there may be extra data
    if len(payload) > 2 and payload[2].strip() != "":
      my_spi.fade(value, float(payload[2]))

    else:
      my_spi.fade(value)

def ctrl_c_quit(signal, frame):
  global toggle_looping
  #tell SPI to stop the thread
  my_spi.end_looping_thread()

  #stop the toggle checking loop
  toggle_looping = False
  print "so long, buddy!"
  sys.exit(0)

def check_toggle():
  #loop!
  while toggle_looping:
    #is the toggle button pressed?
    if GPIO.get_toggle_pressed():
      #is the primary color off?
      if my_spi.primary_color != "000000":
        my_spi.color("000000")
      else:
        my_spi.color(settings.on_color)

    time.sleep(0.05)

  return

def on_connect(client, userdata,flags, rc):
  global connected
        connected = True
  print "--connected--"
  print client
  print userdata
  print rc
  print "--end--"
  start_loop(client)
  
def on_disconnect(client, userdata, rc):
  global connected
  #client.stop_loop()
  connected = False
  print "--disconnect--"
  print client
  print userdata
  print rc
  print "--end--"
  while not connected:
    time.sleep(2)
    client.reconnect()

def start_loop(client):
  #start listening bub
  client.loop_forever()
  

'''
start the script!
'''
#is there a toggle gpio?
if hasattr(settings,"gpio_toggle") and not testing:
  #set up the pin for accepting input
  GPIO.set_toggle_pin(settings.gpio_toggle)

  #poll the toggle switch in a new thread
  toggle_looping = True
  toggle_thread = threading.Thread(target=check_toggle)
  toggle_thread.start()

''' do MQTT stuff '''
#create a client
mclient = mqtt.Client()

#connect on_message
mclient.on_message = on_message

mclient.connect(settings.broker, settings.port, settings.keepalive)
print "connected to broker..."

for topic in settings.topics:
  mclient.subscribe(topic, settings.qos)
  print "subscribed %s %s" % (topic, settings.qos)

#connect ctrl+c to the quit function
signal.signal(signal.SIGINT, ctrl_c_quit)

#track disconnect
mclient.on_disconnect = on_disconnect

#start_loop
start_loop(mclient)



