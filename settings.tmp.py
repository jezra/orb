### Settings ###

#What /dev device is the spi device to write to?
spidev="/dev/spidev1.0"

#how many pixel LEDs are there?
pixels = 1

#what GPIO pin should be used for a toggle switch? (comment out if not using a toggle)
gpio_toggle = 1

#what is the "on" color?
on_color = "FFBB33"

#what is the name/location of the broker
broker="broker.example.com"

#what port is the broker listening on? default is 1883
port=1883

#how often to ping to keep the connection alive?
keepalive=60

#what topics should this orb be subscribed to?
topics = ["light/orb", "lights"]


#what quality of service?
qos=0
