# Orb
Orb is a MQTT client that listens for commands that will then control a ws2801 pixel LED. 

See <http://jezra.net/post/2017-01-02_another_orb_device.html> and <http://www.jezra.net/blog/orb> for an example usage. :)

## Requirements
python
[Paho](https://pypi.python.org/pypi/paho-mqtt) python library for MQTT

## Setup and configure

1. mv settings.tmp.py to settings.py
2. edit settings.py to match your MQTT broker and topic needs
4. run ./orb.py 

## API
Orb listens for a MQTT message string containing ACTION (/ HEXCOLOR) (/VARS) commands. HEXCOLOR should be in RRGGBB format.

### Turning the Orb on or off
Off: `"off"`  
On: `"on"`


### Setting the orb a solid color
Off: `"color/000000"`   
Red: `"color/FF0000"`   
Green: `"color/00FF00"`   
Blue: `"color/0000FF"`   
Pumpkin Orange: `color/F87217`  


### Interrupting the current color with an alert flash
Red: `"alert/FF0000"`   
Green: `"alert/00FF00"`   
Blue: `"alert/0000FF"`   


By default, the orb will flash the alert color 5 times and then return to the previous color. 
#### Set the number of flashes in an alert    
Red: `"alert/0000FF/25"`   

### Blinking a certain color continuously
Red: `"blink/FF0000"`  

By default, `blink` will  cycle between .5 seconds of the HEXCOLOR and .5 seconds of Off (000000).  

#### Set the time a blink color is on in seconds
On for .1 seconds
Pumpkin Orange: `blink/F87217/.1`    

On for 2 seconds
Blue: `"blink/0000FF/2"`

##### set the time a blink color is on and off
on for .1 seconds, off for .75 seconds
Blue: `"blink/0000FF/.1/.75"`

### Fading to a certain color
Blue: `"fade/0000FF"`   

Default fade time is 3 seconds

#### Fade over a number of seconds

Off: `"fade/000000/120"` oh, the movie is starting :)
