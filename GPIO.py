# method to set up a GPIO pin for usage
# number: integer value representing the name of the gpio pin
# direction: string value of either "in" or "out"

toggle_previous  = None
toggle_value  = None
toggle_pin_path = None
toggle_value_path = None
toggle_initial_value = None

def set_pin(number, direction):
  #which global values get set?
  global toggle_value_path
  global  toggle_initial_value

  #convert the number to a string
  num = str(number)
  #unexport the pin by writing the pin number to /sys/class/gpio/export
  f = open("/sys/class/gpio/unexport",'w')
  f.write(num)
  f.close()

  #export the pin by writing the pin number to /sys/class/gpio/export
  f = open("/sys/class/gpio/export",'w')
  f.write(num)
  f.close()

  toggle_pin_path = "/sys/class/gpio/gpio"+num
  toggle_value_path = toggle_pin_path+"/value"

  #get the initial value of the toggle
  toggle_initial_value = get_toggle_value()

  #set the direction
  f = open(toggle_pin_path+"/direction", "w")
  f.write(direction)
  f.close()

#set the toggle pin
def set_toggle_pin(number):
  set_pin(number, "in")

#get the toggle value
def get_toggle_value():
  f = open(toggle_value_path)
  val = f.read()
  f.close()
  return val

def get_toggle_pressed():
  global toggle_value

  #default to returning False
  ret_val = False
  #track the current value
  toggle_previous = toggle_value
  #get the new value
  toggle_value = get_toggle_value()


  #are the values different?
  if toggle_value != toggle_previous:
    #is the value NOT equal to the initial value
    if toggle_value != toggle_initial_value:
      ret_val = True

  #return the retval
  return ret_val


