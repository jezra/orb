#!/usr/bin/env python
import sys
import time
import settings
import color_helper
import threading

class SPI:
  testing = False
  looping_mode = True
  off_color = "\x00\x00\x00"
  hexcolor = off_color
  fading = False
  primary_color = "000000"

  def on(self):
    if settings.on_color:
      self.color(settings.on_color)

  def off(self):
    self.color("000000")

  def color(self, hex_color):
    self.end_looping_thread()
    self.hexcolor = color_helper.hex_color_to_hex_bytes(hex_color)
    self.primary_color = hex_color
    if self.hexcolor == None:
      print "Failed to unpack: "+hex_color
      return

    #write the color to the pixel
    self.blit()

  def blink(self, hex_color, on_time=0.5, off_time=0.5):
    self.end_looping_thread()
    #check the color
    blink_color = color_helper.hex_color_to_hex_bytes(hex_color)
    self.primary_color = hex_color
    if blink_color == None:
      print "Failed to unpack: "+hex_color
      return
    '''we want to run do_blink as a separate thread'''
    looping_thread = threading.Thread(target=self.do_blink, args=((blink_color,on_time,off_time),))
    looping_thread.start()

  def fade(self, hex_color, fade_time=3):
    #ignore this if we are already fading
    if self.fading:
      return

    #check the color
    fade_color = color_helper.hex_color_to_hex_bytes(hex_color)
    if fade_color == None:
      print "Failed to unpack: "+hex_color
      return

    #we are fading!
    self.fading = True

    #end any running looping thread
    self.end_looping_thread()

    #how many steps should there be?
    steps = 255
    fade_sleep = fade_time*1.0/steps

    #convert the hex_color to RGB integers
    hr, hg, hb = color_helper.unpack_hex_string(hex_color)

    #get RGB int values for the current primary_collr
    cr, cg, cb = color_helper.unpack_hex_string(self.primary_color)

    #get the difference in steps for each color
    rs = (0.0+hr - cr)/steps
    gs = (0.0+hg - cg)/steps
    bs = (0.0+hb - cb)/steps

    #loop the number of steps and update the current color
    for i in range(steps):
      cr += rs
      cg += gs
      cb += bs
      self.hexcolor = color_helper.rgb_to_hex(int(cr), int(cg), int(cb))
      self.blit()

      time.sleep(fade_sleep)

    #set the final color, just to be sure
    self.hexcolor = fade_color
    self.blit()
    #we are no longer fading
    self.fading = False
    #update the primary color
    self.primary_color = hex_color

  def end_looping_thread(self):
    self.looping_mode = False
    #sleep for a bit and let any running thread end
    time.sleep(0.02)

  def do_blink(self, args):
    #separate the args
    c, on_time, off_time = args

    #we need to loop for a bit
    self.looping_mode = True
    while self.looping_mode:
      #new color
      self.hexcolor = c
      self.blit()
      if self.exit_sleep(on_time):
        return
      #go off
      self.hexcolor = self.off_color
      self.blit()
      if self.exit_sleep(off_time):
        return

    return

  # a sleep function for determining if a thread should return
  # return true if the thread should exit
  def exit_sleep(self, seconds):
    count = 0
    while count < seconds:
      if self.looping_mode:
        time.sleep(0.01)
        count += 0.01
      else:
        return True

    return False


  def alert(self, hex_color, times = 5):
    #record the current color
    current_color = self.hexcolor

    #process the new hex color
    alert_color = color_helper.hex_color_to_hex_bytes(hex_color)
    if alert_color == None:
      print "Failed to unpack: "+hex_color
      return

    #loop a certain number of times
    for i in range(times):
      #new color
      self.hexcolor = alert_color
      self.blit()
      time.sleep(0.1)
      #go off
      self.hexcolor = self.off_color
      self.blit()
      time.sleep(0.1)

    #go back to the original color
    self.hexcolor = current_color
    self.blit()


  def blit(self):
    if self.testing:
      for i in range(settings.pixels):
        print "testing"
    else:
      #open the spi for writing
      f = open(settings.spidev,"w")
      for i in range(settings.pixels):
        f.write(self.hexcolor)
      f.flush()
      f.close()
